# Introduction to programming (work in progress)#

This project is intended as an introduction to programming. No prior programming knowledge is required. We'll be learning how to program in the programming language [Scala](http://www.scala-lang.org/)

### Lessons ###
The lessons are found in src/main/scala/lessons and are enumerated.

1. Assignments - How to store and reuse values
2. Conditionals - Boolean values and if-else statements
3. Loops - How to execute expressions multiple times, the while-loop
4. Arrays and Lists - Basics about containers, no advanced material
5. Types - What are types, how can the compiler use type information in a statically typed language, what is the type hierarchy, how to provide type annotations
6. Fundamental Types - Numbers, Text (String), Characters, Unit
7. Functions - How to define and use functions; higher order functions
8. Lists Revisited - How to use functions to efficiently and elegantly operate on lists

### Exercises ###
The exercises are found in src/main/scala/exercises. The corresponding solutions are found in src/main/scala/solutions

* 4.a. Everything up to lesson 4
* 6.a. Testing your understanding of types and of the implementation of integer types
* 6.b. Operations on strings
* 7.a. Functions

### How do I get set up? ###

#### Compiler ####
* We're using Scala 2.11.8 so a Scala version at least as high is required and can be downloaded from http://www.scala-lang.org/download/
* Since Scala is running on the Java virtual machine we also require the Java development kit (standard edition) jdk SE. Scala 2.11 requires at least java version 1.6, but downloading a newer jdk is recommended. https://java.com/de/download/

#### Editor ####
In order to start programming we require some editor to edit our programs. However, we're going to use a powerful IDE (integrated development environment) which will help us writing code by providing feedback about programming errors, compiling and running our program, inspecting a running program (debugging), providing automatic completion, and much more. 

* We require an IDE that is compatible with Scala. I recommend using [IntelliJ Idea](https://www.jetbrains.com/idea/) with the Scala-plugin and the sbt-plugin. 


#### Build ####
* This is an sbt (Scala build tool) project. The project can be imported into IntelliJ by opening the build.sbt file with the sbt-plugin installed. This will generate the required project.
* In order to run the project from the command line, downloading sbt is required http://www.scala-sbt.org/download.html

### Further read ###
* https://blog.jetbrains.com/scala/2013/11/18/built-in-sbt-support-in-intellij-idea-13/
* https://www.jetbrains.com/help/idea/2016.1/running-applications.html