package lessons.lesson8

import java.util

object ListsRevisited {
  def main(args: Array[String]): Unit = {
    /*
     * Now that we know about functions we're going to revisit Lists from lesson 4. There are a lot of useful
     * functions defined on List that make many common use-cases much simpler (and often faster) than iterating
     * over all elements in a while-loop.
     *
     * The first function we're looking at is 'foreach'. 'foreach' takes an impure function that receives as input
     * each element of the list and does something with it, but it does not return a value. For example, we can
     * print each element of a list:
     */
    List(1, 2, 3, 4, 5).foreach(i => println(i))
    List("Hello", "world", "!").foreach(w => println(w))
    /*
     * Or we could use foreach to manipulate a variable that is defined outside of the function
     */
    var minimum = Int.MaxValue
    List(6, 3, 1, 6, 2, 4, 6, -1, 3, -2, 2, 5).foreach(i => if (i < minimum) minimum = i)
    println("The minimal element in the list is " + minimum)
    println()
    /*
     * One of the most important functions provided by List is 'map'. 'map' takes a function that receives as input
     * each element of the list and returns some value (not necessarily of the same type). Those values are collected
     * and returned in a new list.
     *
     * For example, we can map a List of Strings to a List of Ints where each element represents the length of the
     * corresponding String
     */
    println(List("Hello", "world,", "what", "does", "the", "fox", "say").map(s => s.length))
    /*
     * Another useful function is 'filter'. 'filter' takes a function that receives as input each element of the list
     * and returns either 'true' (if the element should be kept) or 'false' (if the element should be discarded).
     * Boolean functions are also called predicate. The result is a list containing only the elements where the predicate
     * evaluates to true
     */
    println(List(-2, 4, 1, -4, 2, -5, 6).filter(i => i > 0))
    /*
     * 'map' can also be applied to a List of Lists. In the following example we are given a list of lists of numbers
     * and we are discarding every odd number whilst preserving the list-nesting.
     */
    println(List(List(1, 2, 3), List(2, 3, 4), List(1, 3, 5), List(2, 4, 6)).map(l => l.filter(i => i % 2 == 0)))
    /*
     * Yet another useful function is 'foldLeft' which takes a neutral element and returns a function that takes an
     * accumulator-function. For example, we can use foldLeft to sum up numbers in a list:
     */
    println(List(1, 4, 2, 5, 3).foldLeft(0)((accumulator, next) => accumulator + next))
    /*
     * Similarly we can concatenate a list of strings into a single string
     */
    println(List("This", "is", "a", "sentence", "where", "the", "words", "are", "separated", "by", "a", "whitespace").foldLeft("")((accumulator, s) => accumulator + " " + s).tail)
    /*
     * Can you foldLeft a list of Booleans, such that the result is 'true', if all elements in the list are 'true', and 'false' otherwise?
     */
    // println(List(true, true, false, true).foldLeft(....))
    // println(List(true, true, true, true).foldLeft(....))

    println()
    /*
     * Two other useful functions are 'exists' and 'forall' which both take a predicate as their function argument.
     * 'exists' evaluates to 'true' if there is at least one element in the list for which the predicate evaluates to 'true'.
     * 'forall' evaluates to 'true' only if the predicate evaluates to 'true' for all elements in the list
     */
    println(List(1, 2, 3, 4, 5).forall(i => i > 0)) // true, because all elements are greater than zero
    println(List(1, 2, 3, 4, 5).forall(i => i % 2 == 0)) // false, because not all elements are even
    println(List(1, 2, 3, 4, 5).exists(i => i > 0)) // true, because there is an element greater than zero
    println(List(1, 2, 3, 4, 5).exists(i => i % 2 == 0)) // true, because there is an element that is even
    println(List(1, 2, 3, 4, 5).exists(i => i < 0)) // false, because no element is less than zero
    println()

    /*
     * There are other useful functions on lists, such as 'flatMap', 'reduceLeft', 'scanLeft', 'groupBy', or 'partition'.
     * Look them up if you are interested, and try out various functions on lists.
     *
     * One final trick on how looping can be done more elegant than with while-loops: List of numbers can be generated
     * using 'to' or 'until'. Those can be utilized to generate indices or to generally loop a certain amount of times.
     */
    println(0 to 5) // Range(0, 1, 2, 3, 4, 5)
    println(0 until 5) // Range(0, 1, 2, 3, 4)
    val a1 = Array(1, 2, 3, 4, 5, 6, 7)
    val a2 = Array(2, 3, 4, 5, 6, 7, 8)
    (0 until a1.length).foreach(i => a1(i) = a1(i) + a2(i))
    println(util.Arrays.toString(a1)) // [3, 5, 7, 9, 11, 13, 15]
    (1 to 3).foreach(i => println("Macbeth"))
  }
}
