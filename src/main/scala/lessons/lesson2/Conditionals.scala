package lessons.lesson2

object Conditionals {
  def main(args: Array[String]) {
    /*
     * Sometimes, we want code to only be executed if certain conditions are met.
     * For that purpose there exist two values, 'true' and 'false'. They are called boolean values.
     * Expressions inside an 'if'-block are only executed, if the condition evaluates to 'true'
     */
    if (true) {
      println("This expression is executed")
    }
    if (false) {
      println("This expression is not executed")
    }

    /*
     * New boolean expressions can be computed by:
     * - negating a boolean expression, i.e., not 'true' evaluates to 'false', not 'false' evaluates to 'true'
     * - 'and'-ing boolean expressions, i.e., 'true' and 'true' evaluates to 'true', all other 'and'-expressions evaluate to 'false'
     * - 'or'-ing boolean expressions, i.e., 'false' or 'false' evaluates to 'false', all other 'or'-expressions evaluate to 'true'
     * - 'xor'-ing (exclusive or) boolean expressions, i.e.,
     * -- 'false' xor 'false' = false
     * -- 'false' xor 'true' = true
     * -- 'true' xor 'false' = true
     * -- 'true' xor 'true' = false
     */

    println(true && false) // true and false -> prints 'false'
    println(true && true) // true and true -> prints 'true'
    println(true || false) // true or false -> prints 'true'
    println(true || true) // true or true -> prints 'true'
    println(true ^ false) // true xor false -> prints 'true'
    println(true ^ true) // true xor true -> prints 'false'
    println(!true) // not true -> prints 'false'
    println(true && (true || false)) // true and (true or false) -> since (true or false) is true, this prints 'true'
    println() // line break for better readability

    /*
     * Boolean values can also be obtained by comparing other values.
     * - x == y, evaluates to 'true', if x and y are equal (note: the double equal sign is used by Scala, because the single equal sign is reserved for assignments)
     * - x != y, evaluates to 'true', if x and y are not equal
     *
     * Equality is defined for all values in Scala. For certain values, such as numbers, further comparisons are defined:
     * - 3 < 3, evaluates to 'false'
     * - 3 <= 3, evaluates to 'true'
     * - 3 > 2, evaluates to 'true'
     * - 3 >= 2 evaluates to 'true'
     */
    println(3 < 3)
    println(3 <= 3)
    println(3 > 2)
    println(3 >= 2)
    println() // line break for better readability

    /*
     * The if-block can be complemented by an else-block, that is executed if the expression evaluates to 'false'
     */
    if (3 < 3) {
      println("This expression is not executed")
    } else {
      println("This expression is executed")
    }

    /*
     * We can also have multiple conditions using else if, where the optional final else-block is only executed, if all
     * other conditions evaluate to 'false'
     */
    if (3 < 3) {
      println("This expression is not executed")
    } else if (3 <= 3) {
      println("This expression is executed")
    } else {
      println("This expression is not executed")
    }

    /*
     * If-else conditions can be nested. However, code with many nested expressions becomes much harder to understand, so deep nesting should be avoided
     */
    if (3 < 4) {
      println("3 is less than 4")
      if (3 == 4) {
        println("3 is equal to 4")
      } else {
        println("3 is not equal to 4")
      }
    }
    println() // line break for better readability

    /*
     * In Scala, the last expression of an if-block (or an else-block) is returned.
     * So we can write an expression that computes the maximum of two numbers:
     */
    val a = 7
    val b = 3
    val maxAB =
      if (a > b) {
        a
      } else {
        b
      }
    println(maxAB) // prints 7
  }
  println() // line break for better readability

  val c = 8
  // can you write an expression that returns the minimum of a, b, and c?
  // val minABC = if (...
  // ...
  // println(minABC)
}
