package lessons.lesson1

/*
 * There are three different top-level constructs in Scala: object, class, and trait
 * What they mean isn't important for now - just notice that an entry-point to the
 * program needs to be inside an object and must be named 'main'. A program must have
 * an entry point so that it can be run as a stand-alone application.
 */
object VariableAssignment {
  /*
   * This is an entry point into the program. For now, think of a program as a sequence
   * of expressions that get executed from start to finish.
   *
   * What the line 'def main(args: Array[String]): Unit' means isn't important to know now.
   * We're just defining an entry point to the program.
   *
   * Syntax explanation for the interested:
   * - Starting with the keyword 'def', we define a method called 'main'
   * - Inside the brackets are the arguments to this method
   * -- in this case, we have one argument named 'args'
   * -- after the first colon, the type of the argument is specified. The type restricts the possible values that can be passed
   * --- the type of 'args' is an array of strings, which basically is a list of text
   * - After the last colon, the return type of the method is specified. In Scala, 'Unit' means nothing is returned
   */
  def main(args: Array[String]): Unit = {

    /*
     * Fundamental in programming is the ability to store values.
     * For that purpose we can introduce named variables and assign values to them.
     */

    var x = 5 // stores the value 5 in the variable 'x'
    println(x) // Let's check. This expression should print 5 to the console
    x = 7 // now, the value 7 is stored in the variable 'x'
    println(x)
    /*
     * The next line is just a comment. We cannot introduce a new variable named x, because x is already defined
     * Try uncommenting the line (removing the '//') - the compiler will tell you that 'x is already defined'
     */
    // var x = 8

    val y = 3 // In Scala, we can distinguish between mutable variables (var) and immutable variables (val)
    println(y)
    /*
     * Since y is defined as immutable, we cannot reassign it. The following expression would make the compiler complain.
     * Side note: Since immutable variable are easier to understand one should prefer using val over var.
     */
    // y = 4

    /*
     * We can read the value of a variable, compute something and assign it to a different variable
     */
    val z = x + y // z should now have the value 10 (since x = 7 and y = 3)
    println(z)

    /*
     * Changing the value of x will not have an effect on the already stored value of z
     */
    x = 3
    println(x) // prints '3'
    println(z) // prints '10', since 'x + y' is not reevaluated after changing x

    /*
     * With variables of 'var'-type, we can also have the same variable on the left and the right hand side of the assignment.
     * Any occurrence on the right hand side will use the old value
     */
    x = x + 1 + x // x is now 7 (3 + 1 + 3)
    println(x)

    /*
     * We can also store other things than numbers in our variables.
     * For example, we can store text, or even complex things like lists, dates, animals, ...
     */
    val someText = "This is a text"
    println(someText) // prints 'This is a text' to the console

    val someList = List(1, 1, 2, 3, 5, 8)
    println(someList) // prints 'List(1, 1, 2, 3, 5, 8)' to the console
  }
}
