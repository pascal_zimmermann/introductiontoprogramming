package lessons.lesson4

import java.util

object ArraysAndLists {
  def main(args: Array[String]) {
    /*
     * Arrays are low-level data containers used to store multiple values.
     * In Scala, arrays are 0-indexed, meaning that element 0 is the first element.
     *
     * In Scala, values in the array can be retrieved by putting the index in brackets.
     * There's more going on under the hood, but this is not important right now.
     *
     * The following expressions create a new Array, containing the values 1,1,2,3,5, and 8,
     * and prints each of those elements to the console by accessing it inside a while-loop:
     */
    val a = Array(1, 1, 2, 3, 5, 8)
    var index = 0
    while (index < a.length) {
      println(a(index))
      index = index + 1
    }
    println() // line break for better readability

    /*
     * Elements that are not within the range [0, a.length) cannot be accessed.
     * Trying to access any such element will throw an ArrayIndexOutOfBoundException
     *
     * Try uncommenting any of the following lines to see what happens when you run the program
     */
    // a(-1)
    // a(6)

    /*
     * Elements in the array can be reassigned a different value. Say we want to increment
     * each element in the array by 1:
     */
    index = 0
    while (index < a.length) {
      a(index) = a(index) + 1
      index = index + 1
    }
    println(util.Arrays.toString(a)) // prints '[2, 2, 3, 4, 6, 9]'

    /*
     * Note that, even though the variable 'a' is defined as an immutable val we changed the values that are stored.
     * The reason is that only the reference to the array is immutable, so we cannot make 'a' reference a different
     * array, but the values contained in the referenced array can be changed, because arrays are not immutable.
     *
     * In Scala, arrays are rarely used. One reason is that they are mutable and therefore harder to understand.
     * An other reason is that they are very low-level and do not support many useful operations. However, they are
     * fundamental building blocks in many programming languages and are very fast, meaning the elements are accessed
     * fast (given we know the index).
     *
     * Because arrays are a low-level data structure, we cannot easily add a new element to the array. While existing
     * elements can be overwritten with new values, we cannot enlarge the array to contain a new element.
     *
     * A solution to this are lists, which are also a container for values where the elements are also ordered (i.e.,
     * can be accessed by an index) but support adding new values. In Scala, the default List is immutable (there exist
     * also mutable variants), so adding a new element to the list will not add the element to the existing list, but
     * rather create a new list with the new element.
     */
    var l = List(1, 1, 2, 3, 5, 8)
    println(l) // prints 'List(1, 1, 2, 3, 5, 8)'
    l = l :+ 13 // :+ is a method of List, defined as creating a new list with the element appended
    println(l) // prints 'List(1, 1, 2, 3, 5, 8, 13)'
    println() // line break for better readability

    /*
     * However, note that adding new elements at the last position is slow for this particular implementation of List.
     * Fortunately, appending elements is usually not necessary, as we can also prepend elements which is very fast for
     * this implementation of List. The method :: (colon colon) of List creates a new list with the element on the left
     * side prepended.
     */
    l = 13 :: List(8, 5, 3, 2, 1, 1)
    println(l) // prints 'List(13, 8, 5, 3, 2, 1, 1)'
    l = 21 :: l
    println(l) // prints 'List(21, 13, 8, 5, 3, 2, 1, 1)'
    println() // line break for better readability

    /*
     * Since this implementation of List is immutable, we cannot directly change existing values, but need to create a
     * new list, if, e.g., we want to increment each element by one.
     */
    var incrementedList: List[Int] = List() // Side-note: We need to explicitly specify the type of the content of the List, here Int, because for an empty list, the compiler does not know what we want to put in
    var counter = 0
    while (counter < l.length) {
      incrementedList = l(counter) + 1 :: incrementedList
      counter = counter + 1
    }
    println(incrementedList) // prints 'List(2, 2, 3, 4, 6, 9, 14, 22)'
    /*
     * Note that the elements in 'incrementedList' are reversed, because we accessed the elements of 'l' from first to last, but prepended (not appended) the incremented value
     * We can correct this by calling 'reverse' on the list
     */
    incrementedList = incrementedList.reverse
    println(incrementedList) // prints 'List(22, 14, 9, 6, 4, 3, 2, 2)'

    /*
     * Teaser:
     * Using immutable lists seems to be unnecessary complicated, however immutable data structures are much easier to
     * understand in more complex programs. Furthermore, functional programming exploits immutability and makes many
     * real use-cases very simple and elegant. For example, the following achieves the same thing: Incrementing each element by one
     */
    val alsoIncrementedList = l.map(_ + 1) // creates a new list with each element incremented by 1.
    println(alsoIncrementedList) // prints 'List(22, 14, 9, 6, 4, 3, 2, 2)'
    println() // line break for better readability

    /*
     * Not only numbers can be put into arrays and lists. As with assignments, anything can be inserted.
     */
    val colors = List("Red", "Blue", "Green", "Purple", "Yellow")
    println(colors) // prints 'List(Red, Blue, Green, Purple, Yellow)'

    /*
     * Lists can also be queried for the number of elements contained inside (as used above) or for emptiness.
     * Furthermore, the method 'head' can be used to access the first element of the list (just like we could use list(0)),
     * and 'tail' can be used to create a new list from all but the first element. If the list contains only one element,
     * 'tail' will return an empty list.
     * For example, we can consume a list until it is empty as follows
     */
    var listToConsume = colors
    while (listToConsume.nonEmpty) {
      println(listToConsume.head)
      listToConsume = listToConsume.tail
    }
    println() // line break for better readability

    /*
     * Try to write code that computes the sum of each elements in the list 'l'
     */
    l = List(21, 13, 8, 5, 3, 2, 1, 1)
    var sumOfL = 0
    // while(...)
    // ...
    println(sumOfL) // shall print 54
    println() // line break for better readability

    /*
     * It is also possible to put lists inside lists (or arrays inside arrays, or lists inside arrays, and so on)
     */
    val evenAndOdd =
      List(
        List(1, 3, 5, 7, 9),
        List(2, 4, 6, 8)
      )
    println(evenAndOdd) // prints 'List(List(1, 3, 5, 7, 9), List(2, 4, 6, 8))'

    /*
     * With the basics from the first four lessons you can try to solve exercise 4a
     * The exercise is located under exercises.exercise4a
     */

  }
}
