package lessons.lesson7

object Functions {
  def main(args: Array[String]): Unit = {
    /*
     * In programming, functions provide us with a means of abstracting lower level implementation into a higher level
     * building block. They also allow us to reuse code, because the same function can be applied to multiple values.
     *
     * You've probably already heard about functions in your math classes. For example, suppose we have a function
     *   f: N -> N
     *   f(x) = x + 1
     * often pronounced as 'f, a function from the set of natural numbers to the set of natural numbers. f of x is equal
     * to x plus one'. A function always has a number of function arguments (here, 'x') and defines a mapping for each
     * valid input value to a unique output value (here, 'x + 1'). A mathematical function cannot map the same input to
     * multiple outputs, but multiple inputs can be mapped to the same output, as is the case in
     *   f: Z -> N
     *   f(x) = x * x
     * where f(1) = f(-1) = 1.
     *
     * But enough with the theory for now. Let us implement the f(x) = x + 1 in Scala, but let us call this function
     * increment, because giving our variables meaningful names is important.
     */
    val increment: Int => Int = x => x + 1
    /*
     * We have stored our function in a variable called increment. The type of this variable is 'Int => Int' which means
     * it is a function that takes an argument of type 'Int' and returns a value of type 'Int'. The function itself is
     * expressed as 'x => x + 1' which means 'map x to x + 1'.
     * Our function can now be applied to any expression of type 'Int':
     */
    println(increment(0)) // prints '1'
    println(increment(10)) // prints '11'
    println(increment(-32)) // prints '-31'
    println(increment(increment(increment(1)))) // prints '4'
    println()
    /*
     * Functions can also have multiple arguments. Consider the function 'area' which takes the width and the length
     * of a rectangle and returns the area covered by the rectangle.
     */
    val area: (Int, Int) => Int = (width, length) => width * length
    println(area(3, 4))
    println(area(5, 7))
    println(area(increment(3), 19))
    println()
    /*
     * Functions can be defined on any type. The function body is also not restricted to a single expressions.
     * Multiple expressions can be executed in sequence by surrounding them with curly braces. In this case, the result
     * of the last expression is returned as the result of the function.
     */
    val containsChar: (Char, String) => Boolean = (c, s) => {
      var i = 0
      var result = false
      while (i < s.length) {
        if (s.charAt(i) == c) {
          result = true
        }
        i = i + 1
      }
      result
    }
    println(containsChar('w', "Hello world")) // true
    println(containsChar('w', "Hello World")) // false
    println()
    /*
     * Functions can also take other functions as arguments. Those functions are called 'higher order functions'.
     */
    val reduce: (Array[Int], (Int, Int) => Int) => Int = (ints, f) => {
      var i = 1
      var result = ints(0)
      while (i < ints.length) {
        result = f(result, ints(i))
        i = i + 1
      }
      result
    }
    val add: (Int, Int) => Int = (x, y) => x + y

    println(reduce(Array(1, 2, 3, 4, 5), add))
    /*
     * Here, we have defined a higher order function reduce that takes an array of Int ints and a function f, that itself
     * maps two Int values to another Int value, and applies f to the first and the second entry of ints, and again to
     * the result of f and the next entry of ints for each entry of ints.
     *
     * Try defining different functions, such as 'multiply' that take two Int values and maps them to an Int value. Try
     * applying those functions to 'reduce'.
     */




    println()
    /*
     * Some people distinguish between pure and impure functions. Pure functions are side-effect free and behave just
     * like functions defined in mathematics. This means they are well understood, hence, it is much easier to reason
     * about the behaviour of the program. All functions that we have defined up to here can be considered pure.
     *
     * However, in Scala we are not restricted to defining pure functions. Say we want to define a function that takes
     * an array of Int values and prints each of the elements. This function isn't supposed to return a value, but it
     * has a side-effect: It displays some output on screen. In Scala, a function that does not return a value is typed
     * as returning a value of type 'Unit'. Recall that the only value of type Unit is '()'.
     */
    val printAll: Array[Int] => Unit = values => {
      var i = 0
      while (i < values.length) {
        println(values(i))
        i = i + 1
      }
    }
    printAll(Array(4, 2, 1, 5))
    println()
    /*
     * Functions can also return a function. Let's define a function that takes an Int and returns a function. Can you
     * figure out what the function 'fold' is supposed to do? If not, try looking at 'reduce' again.
     */
    val fold: Int => (Array[Int], (Int, Int) => Int) => Int = x => {
      (ints, f) => {
        var result = x
        var i = 0
        while (i < ints.length) {
          result = f(result, ints(i))
          i = i + 1
        }
        result
      }
    }

    println(fold(4)(Array(1, 2, 3), add)) // fold(4) returns a function that takes an array of Int and a function. Inside the second pare of brackets we pass fitting arguments to the result of fold(4)
    println(fold(6)(Array(), add)) // unlike our function 'reduce', 'fold' also works with empty arrays
    /*
     * There's no need to store functions inside a variable. We can also write them wherever an expression of a corresponding
     * function-type is expected. We are even allowed to omit the type annotation, because the Scala compiler can infer
     * the type.
     */
    println(fold(6)(Array(1, 2, 3), (x, y) => x - y))

    /*
     * Functions are a very important concept, so you should obtain a solid understanding. Try to come up with other
     * functions in the spirit of this lesson.
     * If you feel ready, try solving exercise7a
     */

  }
}
