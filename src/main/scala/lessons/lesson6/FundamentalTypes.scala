package lessons.lesson6

import java.util

object FundamentalTypes {
  def main(args: Array[String]): Unit = {
    /*
     * This lesson will provide an overview over some of the most basic types.
     *
     * Previously, we've seen that Boolean expressions represent a truth value
     * that can either be 'true' or 'false'. They are a direct subtype of AnyVal.
     *
     * We've also already used numbers. One of the most common numeric types is 'Int',
     * which represents 32-bit signed integers. Hence, only 2^32 = 4'294'967'296 different
     * values can exist for 'Int'. However, since we're able to represent positive and negative
     * numbers, the maximum number that can be represented as an 'Int' is only about half this big number.
     */
    val maxInt = Int.MaxValue
    println(maxInt) // the highest number that fits into 'int' is 2147483647
    /*
     * If we add one more to the maximum value, then the value will overflow and we're getting the minimal
     * value that fits into an Int
     */
    println(maxInt + 1) // -2147483648
    /*
     * Did you realize that the minimal value is one less than the negated maximum value? The reason is that
     * the value 0 is neither positive nor negative, so in order to represent 0 we require one bit.
     *
     * Let's inspect how a positive number, e.g., 21, is represented when broken down into bits
     */
    println(21.toBinaryString)
    /*
     * So 21 is represented in bits as 0000 0000 0000 0000 0000 0000 0001 0101
     * Up to the first bit those represent powers of two, where the last bit represents 2^0, the last but one represents
     * 2^1, and so on, up to 2^30. The first bit represents -2^31. Adding those numbers we can represent any value
     * between -2^31 and (2^31 - 1). In our example, we have 2^0 + 2^2 + 2^4 = 1 + 4 + 16 = 21.
     *
     * Let's also verify the representation of negative numbers.
     */
    println((-21).toBinaryString)
    /*
     * Now this prints 1111 1111 1111 1111 1111 1111 1110 1011, which means 2^0 + 2^1 + 2^3 + 2^5 + 2^6 + ... + 2^30 - 2^31
     *
     * If we would need to represent larger (or smaller) numbers, then, instead of 'Int', we can use the 'Long' type. Long
     * values are implemented the same as Int values, but they are represented using 64 bits instead of 32.
     * Long stands for 'long word' as it uses twice as many bits as Int
     * Keep two things in mind though:
     * 1. Just because expressions of type Long can represent a much larger range of values doesn't mean they cannot overflow
     * 2. On 32 bit systems operations on Long values will use more CPU cycles than operations on Int values - they also require two 32-bit registers to be stored
     */
    println(Long.MaxValue) // 9223372036854775807 (2^63 - 1)
    println(Long.MaxValue + 1) // -9223372036854775808 (-2^63)

    /*
     * Note that Int is not a Subtype of Long (Even though in theory it could be). However, there exist implicit conversions
     * so an Int can be upgraded to a Long. This also happened in the line above, as we're adding the Int-value '1' to the
     * Long value '9223372036854775807'
     */

    /*
     * The next important type we're going to look at is 'Char'. 'Char' is really just an unsigned Byte, but is used to
     * represent actual characters. Char-literals are denoted by single-quotes
     */
    val thisIsAChar: Char = 'f'
    println(thisIsAChar)
    println('A')
    println('&') // Char is not restricted to letters only, but to ASCII characters https://en.wikipedia.org/wiki/ASCII
    println('d' - 'a') // prints 3, because 'd' is three characters away from 'a' (remember, Char is really just a number aswell
    println('d' > 'a') // prints true, because as with other numbers, 'Char' values are comparable

    /*
     * Since single characters are usually not that useful, text is usually represented as a value of type 'String', which
     * basically can be thought of as an Array of Char. Other than 'Char', 'Int', 'Long', and 'Boolean', 'String is not a
     * subtype of 'AnyVal', but of 'AnyRef'.
     */
    val s: String = "This is a String. A String is used to represent text"
    println(s)
    /*
     * The type 'String' provides us with a lot of useful operations.
     * - We can get the number of characters in the string using the method '.length'
     * - We can take part of the String using the method '.substring' providing the start-index (0-based) and the end index
     * - We can convert the String into a basic Array of Char using the method '.toCharArray'
     * - We can convert each character in the string to its uppercase equivalent using the method '.toUpperCase' (same for lowercase)
     * - Strings are comparable by their lexicographical ordering
     * - We can split a String into an Array of Strings at a certain character, e.g., at the whitespace
     * - We can compare two Strings using equality which is defined on 'Any' (==)
     * - many more
     */
    println(s.substring(10, s.length)) // prints 'String. A String is used to represent text'
    println(util.Arrays.toString(s.toCharArray)) // [T, h, i, s,  , i, s,  , a,  , S, t, r, i, n, g, .,  , A,  , S, t, r, i, n, g,  , i, s,  , u, s, e, d,  , t, o,  , r, e, p, r, e, s, e, n, t,  , t, e, x, t]
    println(s.toUpperCase()) // THIS IS A STRING. A STRING IS USED TO REPRESENT TEXT
    println("Apple" > "Pear") // false (lexicographical ordering)
    val words: Array[String] = s.split(" ")
    words.foreach(println) // print each word to a separate line
    println("Apple" == "Apple") // prints true
    /*
     * There are a lot of predefined String operations and you should get familiar with most of them.
     *
     * One final important note about String before we move on: Strings are immutable, meaning there is no operation
     * that will change the value of an existing String.
     */

    /*
     * In Scala, there exists another important type called 'Unit'. The type accepts only one value which is denoted
     * as an opening and a closing bracket '()'. The Unit-Type is used to specify that an expression does not evaluate
     * to an actual value. This very method (main) is an example of a method that does not return an actual value. However,
     * the meaning of the Unit-type will only become apparent once we introduce methods and functions.
     */
    val theUnitValue: Unit = ()
    println(theUnitValue)

    /*
     * We've seen a lot of types already. However, at least one very important type is missing, decimal numbers. There
     * exist two basic types, 'Float' - the single precision floating point numbers, and 'Double', the double precision
     * floating point numbers. As with 'Int' and 'Long', they are implemented using 32 or 64 bits respectively.
     */
    println(2.1)
    val d: Double = 7.2
    val f: Float = 2.4f // appending an f to the number-literal will force the floating-point number to fit into 32 bit
    println(d)
    println(f)
    /*
     * They both can represent large numbers, but by nature many of the possible numbers cannot be represented, because
     * there are already infinitely many numbers between 0 and 1.
     *
     * There's also a special value to represent numbers that are too large and don't fit into the range of Double. The
     * value is called 'Infinity'. So computations on Double will not overflow. Furthermore, there is a special value
     * 'NaN' (not a number) for computations that are not defined such as 0/0.
     */
    println(Float.MaxValue)
    println(Double.MaxValue)
    println(Double.MaxValue * Double.MaxValue)
    println(0.0 / 0.0)
    /*
     * This error based on the precision is something we always have to keep in mind. Consider this very simple example:
     * The square-root of 3 times the square-root of 3 is equal to 3. However, this simple computation already fails to
     * evaluate to true because of the error. Guess what gets printed in the following if-expression!
     */
    if (Math.sqrt(3.0) * Math.sqrt(3.0) == 3.0) {
      println("As expected, the square of the square-root is the number itself")
    } else {
      println(Math.sqrt(3.0) * Math.sqrt(3.0))
    }
    /*
     * Because of the severity of those errors, the 32-bit Float is rarely used compared to the 64-bit Double.
     * Exceptions to this are for example computation-heavy applications where errors don't matter as much, such as many
     * graphical applications.
     *
     * However, never use either of those two types when precision matters, e.g, when representing currency. There
     * are other custom types available for those kinds of computations, but they are much more expensive on the CPU.
     */

  }
}
