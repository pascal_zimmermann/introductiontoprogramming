package lessons.lesson9

/*
 * In this lesson we are going to introduce the concept of classes. Think of a class as a blueprint from which we can
 * forge values at runtime. Classes are one of the most important concepts with the goal of providing higher level
 * abstractions (read this as: goal of reducing complexity in your program, or making programs easier to understand).
 * It will take a lot of practice to understand classes, even though we're only introducing some fundamentals here. If
 * the classes introduced in this lesson are too hard to understand, try drawing a picture of them on a piece of paper,
 * or try coming up with your own classes (and implementing them).
 *
 * In this lesson we will cover
 *  - How to define classes
 *  - Link between classes and types
 *  - How to create a new value of a class
 *  - How to define a subclass; the concept of subtyping and inheritance
 *  - What methods are and how they can be defined on classes; how inherited methods can be redefined
 *  - The notion of equality
 *  - Difference between mutable and immutable classes
 *  - Short introduction to what a reference is and their impact when using mutable classes
 *
 *
 * Let's get started!
 * 
 * Let us define a class called 'Animal'. Classes are defined by using the keyword 'class' followed by the name
 * we want to give our class. Any new class we introduce in our program also introduces a new type of the same name.
 * So by defining a class 'Animal', we have now also a type 'Animal'. By default, any new type introduced like this
 * is a subtype of 'AnyRef' so it gives us all the guarantees that 'AnyRef' gives us (most prominently, we can call
 * 'toString', 'hashCode', and we can compare it to other values using '==').
 */
class Animal

/*
 * Let's see what we can do with our Animal. Here starts the entry point to our program:
 */
object Classes {
  def main(args: Array[String]) {
    /*
     * A value of type Animal can be created using the keyword 'new', i.e., 'new Animal' will give us a new value of
     * type 'Animal'. From now on we will be calling these kinds of values either 'an instance of type Animal' or
     * 'an object of type Animal'.
     */
    val animal = new Animal
    /*
     * As mentioned, we can call 'toString' on the animal object or compare it to other objects. Let's find out what
     * those do:
     */
    println(animal.toString)
    /*
     * For me, this printed 'lessons.lesson9.Animal@3079279'. What's incorporated in this String is somehow the type
     * and a representation of the memory location this object is stored. Usually, this String representation of the
     * object is not very useful. We'll see later in this lesson how a more useful String representation can be defined.
     */
    val otherAnimal = new Animal
    val maybeAnimal: AnyRef = new Animal
    val sameAnimal = animal
    println(animal == animal)
    println(animal == otherAnimal)
    println(maybeAnimal == animal)
    println(maybeAnimal == "Animal")
    println(sameAnimal == animal)
    /*
     * By default, equality is defined as 'reference equality', meaning that two objects are equal only if they are
     * exactly the same object. So our object animal is equal to itself, but it is not equal to otherAnimal, a different
     * object. It is also equal to the object stored in the variable 'sameAnimal', because the same object is stored in
     * both variables. This concept of references is very important to understand as you will soon see.
     *
     * We are now introducing a new class which we'll name 'Location'. Scroll down to the class 'Location' and continue
     * reading there before returning here.
     */
    println()

    /*
     * Do you understand the implementation of 'Location'? Let's test our implementation.
     *
     * Unlike 'Animal', 'Location' now has constructor arguments which need to be passed when creating a new object.
     */
    val london = new Location(52, 0)
    val newYork = new Location(41, -74)
    val manhattan = new Location(41, -74)
    println(london)
    println(newYork)
    /*
     * As you can see, the string representation is much more useful as we can now distinguish the two locations.
     * 'toString' is often used for debugging your program, so remember to write a meaningful string representation when
     * introducing new classes.
     */
    println(london == newYork) // false
    println(london == london) // true
    println(newYork == manhattan) // also true, because they have the same longitude and latitude, even though they are different objects

    /*
     * We are going to introduce some more types. Scroll down and continue reading where we're implementing 'Plant'
     */
    println()

    /*
     * Let's try out our plants. If you're not sure what's going on, try inventing and implementing other plants, define
     * more methods in the current interface and use the instances of Plant in other ways.
     */
    val apple: Apple = new Apple
    println(apple) // we have instantiated a new apple - prints lessons.lesson9.Apple@1786b2ca for me
    val grownAppleTree: AppleTree = apple.reproduce // because we have overridden 'reproduce', an AppleTree is returned
    println(grownAppleTree) // lessons.lesson9.AppleTree@11c0f73a
    val moreTrees: List[Plant] = grownAppleTree.fruits.map(f => f.reproduce) // Our apple tree has two apples. Let's regrow a tree from each apple
    println(moreTrees) // List(lessons.lesson9.AppleTree@74ee003d, lessons.lesson9.AppleTree@3551e0c4)
    val fruits: List[Fruit] = List(new Apple, new Coconut, new Fruit)
    println(fruits.map(f => f.reproduce)) // only the reproduce-method in Apple is overridden, so this produces an AppleTree and two Plants - List(lessons.lesson9.AppleTree@10094da, lessons.lesson9.Plant@4d2c74b9, lessons.lesson9.Plant@21a34544)

    /*
     * Up to now all our classes were immutable. To finish off this lesson, we are going to introduce a mutable class.
     * This is not an entirely new concept, but it's important to understand how mutable objects work. Scroll down to
     * where 'IntStack' is introduced.
     */
    println()
    /*
     * Now let's try out our IntStack
     */
    val stack = new IntStack
    println(stack.isEmpty) // a newly created stack is empty
    stack.push(10) // now, 10 is on top of the stack
    println(stack.isEmpty) // the stack is no longer empty
    println(stack.pop()) // removes 10 from the stack and prints 10
    println(stack.isEmpty) // now, the stack is empty again
    List(1, 2, 3, 4).foreach(i => stack.push(i)) // on top of the stack is now 4, on the bottom is 1
    while (!stack.isEmpty) {
      // print all elements until the stack is empty
      println(stack.pop())
    }
    println(stack.isEmpty) // the stack is now empty
    val sameStack = stack  // store the same reference (i.e., the same stack) into variable 'sameStack'
    sameStack.push(5)
    stack.push(2)
    while(!stack.isEmpty) {
      println(stack.pop()) // prints 2 and then 5, because we only have one instance of stack, even though they are two different variables
    }

  }
}

/*
 * Here, we are introducing a class 'Location' which represents a location on earth by storing the longitude (between
 * -180° and 180°) and the latitude (between -90° and 90°). The longitude and the latitude are stored as Int-variables.
 * Those variables are introduced within a pair of brackets after the name of the class. They are also commonly referred
 * to as 'constructor arguments', because they are required to be set when a new object of type 'Location' is instantiated.
 *
 * As with Animal, Location is a subtype of AnyRef and provides us with default implementations for 'toString', 'hashCode',
 * and equality. However, as we've seen before, those default implementations may not be very useful. We can redefine the
 * implementation; this is called 'overriding'.
 */
class Location(val latitude: Int, val longitude: Int) {

  /*
   * Suppose we want the location to be represented like 'Location(52° N, 0° W)'
   *
   * We override AnyRefs implementation of 'toString' by redefining the same name as a new def but with the 'override'
   * keyword. We haven't introduced 'def' yet, but you can think of it as being the definition of a function. In fact it
   * is even possible to create a function from a 'def', e.g., for storing in a variable or passing to a different function.
   * The construct introduced by the 'def'-keyword is commonly called 'method'. Unlike a function, a method is not required
   * to take parameters as is the case with 'toString'. For a method to override the method introduced in the parent-type,
   * the name of the method as well as the number, order, and type of the input arguments, and also the return type must
   * correspond to those defined in the parent-type. (Side-note: This is not the full truth. In fact, we are allowed to
   * change the types but there are constraints to what we are allowed to do, so let's keep it simple for now)
   */
  override def toString: String = {
    val longitudeSymbol = if (longitude < 0) "W" else "E"
    val latitudeSymbol = if (latitude < 0) "S" else "N"
    "Location(" + Math.abs(latitude) + "° " + latitudeSymbol + ", " + Math.abs(longitude) + "° " + longitudeSymbol + ")"
  }

  /*
   * Let's also override equality, such that two locations are the same if their longitude and their latitude are the same.
   *
   * Overriding equality is special, because we're comparing two objects using '==', but we're actually overriding a method
   * called 'equals'. The reason for this is fairly plain: It's necessary for interoperability with Java.
   *
   * 'equals' has an input argument of type 'Any' and returns a boolean, 'true', if the objects are equal and 'false' if
   * they are not. The reason the input argument is of type 'Any' is that two objects need not necessarily be of the same
   * type to be equal - depending on how we want to define the equality. However, equality is supposed to be symmetric,
   * i.e., a == b should yield the same result as b == a. As obvious as this property sounds it is very easy to implement
   * this wrongly.
   */
  override def equals(other: Any): Boolean = {
    /*
     * Because 'other' is of type 'Any', we cannot directly access the longitude and latitude of 'other'. In fact, we do
     * not know whether 'other' is of type 'Location' or not. Fortunately we can check the type information at runtime
     * by calling 'isInstanceOf[Location]', and treat it as if it were a Location using 'asInstanceOf[Location]'. Scala
     * also provides a cleaner way of doing this called pattern-matching, but we're not going to introduce pattern matching
     * in this lesson.
     */
    if (other.isInstanceOf[Location]) {
      longitude == other.asInstanceOf[Location].longitude && latitude == other.asInstanceOf[Location].latitude
    } else {
      false
    }
  }

  /*
   * Don't worry about the use of hashCode for now. Just remember that you shall always override equals and hashCode or
   * neither - otherwise you're prone to encounter strange errors when using e.g., Maps
   */
  override def hashCode: Int = longitude ^ latitude

  /*
   * You can now continue reading where we left the main-method
   */
}


/*
 * Do you remember that the types form a hierarchy? We can extend the type hierarchy by introducing new classes that are
 * subclasses of existing classes. Consider the following example: A plant bears any number of fruits. Fruits are used
 * to reproduce, spawning new plants. Apple trees are plants that bear apples while apples are fruits that spawn apple trees.
 * Palm trees are also plants. They bear coconuts which are fruits. There may also be other plants, but we know nothing
 * about them.
 */
class Plant(val fruits: List[Fruit])

class Fruit {
  /* Let's say a fruit grows a new plant that has no fruits. The newly grown plant is a new instance of Plant */
  def reproduce: Plant = new Plant(Nil)
}

/*
 * Now let us introduce a more specific type of Plant and Fruit, namely AppleTree and Apple. Using the syntax
 * 'class X extends Y', we define a new class 'X' that is a subclass of 'Y'. There are three terms associated with this
 * concept that you must learn:
 * - subtyping: As you know, a new class also introduces a new type with the same name. Since we say that 'Apple extends Fruit',
 *              the newly introduced type 'Apple' is a subtype of 'Fruit'. Recall from lesson 5 that this means that
 *              an Apple can be used where a Fruit is expected and that Apple must give us at least the same guarantees
 *              that a Fruit provides.
 * - inheritance: Remember that Fruit provides a method called 'toString' even though we did not define this method on
 *                Fruit? This is because 'toString' is defined on 'AnyRef' and 'Fruit' is a subtype of 'AnyRef': Fruit is
 *                inheriting all the methods defined in AnyRef. Now, Apple is also inheriting all methods defined in Fruit,
 *                and also all methods defined in the supertype of Fruit (AnyRef). We are allowed to redefine inherited methods,
 *                but we are not obliged to do so.
 * - subclassing: Subclassing is simply the combination of subtyping and inheritance. Subtyping and inheritance are often
 *                used together, but they are independent concepts.
 */
class Apple extends Fruit {
  /*
   * Let us redefine the inherited method 'reproduce' to return not just an arbitrary plant, but an apple tree, one that
   * bears already two new apples. Notice how we changed the return type from Plant to AppleTree. This is possible because
   * AppleTree is a subtype of Plant - which means that the return type gives us at least the same guarantees as the return
   * type of the overridden method. i.e., a value of a subtype can be used wherever a value of the parent type is expected.
   */
  override def reproduce: AppleTree = new AppleTree(List(new Apple, new Apple))
}

/*
 * Since Plant has a constructor argument, we must pass a value when extending Plant. In this case we have defined a
 * constructor argument for AppleTree, a list of apples, which we simply forward to the constructor of the superclass.
 */
class AppleTree(apples: List[Apple]) extends Plant(apples)

/*
 * Let us implement Coconut and PalmTree in a similar fashion, but this time we're not overriding the method 'reproduce'.
 */
class Coconut extends Fruit

class PalmTree(coconuts: List[Coconut]) extends Plant(coconuts)

/*
 * You can now continue reading where we left the main-method
 */


/*
 * Up to now all classes we have defined were immutable, i.e., they only stored values as 'val' not as 'var'.
 * We are now going to implement a class 'IntStack' that supports adding and removing values of type Int. Stacks are
 * very simple data stores that are used to store elements in a 'first-in-last-out' manner. An example would be a
 * cupboard: The clean plates that you put last into your cupboard are the plates you remove first from your cupboard.
 *
 * We're going to support three operations
 * - push: Put a new Int-value on the stack
 * - pop: Remove the last Int-value from the stack and return it
 * - isEmpty: Returns 'true' if there are no more elements to remove.
 */
class IntStack {
  /*
   * The topmost element in the stack stored in a variable named 'head'. Here, we're using the type 'Option' to store the
   * element. Option has two subtypes: 'None' and 'Some'. None means there is no element present, which is the case just
   * after creating a new instance of IntStack, or after we have removed all existing elements. Some simply stores the
   * reference to the existing value, which in our case is of type 'IntStackEntry'.
   */
  var head: Option[IntStackEntry] = None

  /*
   * The stack is empty if 'head' is none, i.e., if 'head' is empty. In this case we are not supposed to call pop
   */
  def isEmpty: Boolean = head.isEmpty

  /*
   * A new element is added to the stack. Notice how we make use of 'head' being stored in a 'var' instead of a 'val'.
   * We reassign 'head' with a newly created instance of 'IntStackEntry', wrapped in a 'Some' to fulfill the type constraint.
   *
   * IntStackEntry is defined to remember what the next element in the stack is, so we can retrieve it later when removing
   * the head. Therefore, we need to store the current head inside the new instance of IntStackEntry.
   */
  def push(element: Int): Unit = head = Some(new IntStackEntry(element, head))

  /*
   * Pop returns the int-value contained in the current head. It also replaces the current head with the head that we
   * previously stored inside the current head when creating it.
   *
   * It's not defined what should be happening if we call pop on an empty stack - in fact, this implementation will run into exceptional behaviour
   */
  def pop(): Int = {
    val result = head.get.element
    head = head.get.next
    result
  }
}

class IntStackEntry(val element: Int, val next: Option[IntStackEntry])

/*
 * Finish this lesson by continuing reading where we left the main-method
 */