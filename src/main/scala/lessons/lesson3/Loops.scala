package lessons.lesson3

object Loops {

  def main(args: Array[String]) {
    /*
     * Sometimes we need our program to execute some expressions multiple times.
     * For example, it is said that if an actor speaks the name 'Macbeth', he should leave the building, spin around three times, spit, and curse to avoid bad luck
     * The most basic method to repeat some expressions in Scala is a 'while'-loop, that gets executed as long as its condition evaluates to 'true'
     */
    println("Saying: 'Macbeth'")
    println("Leaving building")
    var count = 0
    while (count < 3) { // since we're incrementing count at the end of this while-loop, the loop is executed for count = 0, count = 1, and count = 2
      println("Spinning")
      count = count + 1 // careful: if we make a mistake, e.g., forget to increment count, the loop may be executed infinitely
    }
    println("Spitting")
    println("Cursing")
    println() // line break for better readability

    /*
     * In basic mathematics, 5 to the power of 4 means that we multiply 5 four times with itself, i.e., 5 pow 4 = 5 * 5 * 5 * 5
     * Can you compute 5 pow 4 using a while-loop?
     */
    var result = 1
    // while(...) {
    // result = result * ...
    // ...
    println(result) // shall print 625

    /*
     * Like if-else expressions, loops can also be nested. We can even put if-else expressions inside loops and vice-versa
     * However, as already mentioned, deep nesting makes the code much harder to understand
     * Furthermore, nested loops may cause the execution time of the program to skyrocket
     *
     * In mathematics, the factorial of n, denoted by n!, is defined as n * (n-1) * (n-2) * ... * 1
     * The following expression computes (and prints) the factorial of all numbers from 1 to 7
     */
    var n = 1
    while (n <= 7) {
      var factorial = 1
      var factor = 1
      while (factor <= n) {
        factorial = factorial * factor
        factor = factor + 1
      }
      println("The factorial of " + n + " is " + factorial)
      n = n + 1
    }
    println() // line break for better readability

    /*
     * Let's do that again for our power function:
     * Can you compute n pow 4 for n = {1, 2, 3, 4, 5, 6, 7}?
     */

    // Write the code

    /*
     * shall print
     * 1 pow 4 is 1
     * 2 pow 4 is 16
     * 3 pow 4 is 81
     * 4 pow 4 is 256
     * 5 pow 4 is 625
     * 6 pow 4 is 1296
     * 7 pow 4 is 2401
     */

  }
}
