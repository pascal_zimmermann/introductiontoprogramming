package lessons.lesson5

object Types {
  def main(args: Array[String]) {
    /*
     * In this lesson, we're going to learn what types are, what the compiler for a statically typed language can do
     * with type information, what the type hierarchy is, and how we can actively use type annotations in our Scala
     * program.
     *
     * Types are a very important concept. In a typed language all expressions are typed. Think of the type as
     * a guarantee and as a restriction on certain properties of an expression. Let's look an example in Scala:
     */
    var x: Boolean = true
    /*
     * Here, we define a variable 'x' and by explicitly writing the type annotation ': Boolean' we specify, that the type of
     * 'x' is 'Boolean'. The type of 'x' gives a restriction on the possible values that can be assigned to 'x'. There
     * are only two possible values that match type 'Boolean', namely 'true' and 'false'.
     *
     * So we can reassign 'x' with either 'true' or 'false', but we cannot assign something else, such as a number.
     */
    x = false // this is fine
    // x = 25 // This will yield a compile-error: Expression of type Int doesn't conform to expected type Boolean

    /*
     * Why would we want to restrict ourselves? Because type information also gives us a guarantee on what we can
     * do with the expression. If you recall from the previous lessons, a boolean expression can be ANDed with an other
     * boolean expression, yielding true only if both expressions individually evaluate to true.
     */
    if (x && true) {
      //
    }
    /*
     * While the operator && is defined for booleans, it is not defined e.g. for numbers. However, since we have
     * restricted ourselves to only assign boolean values to 'x', we can be sure that later on we can use it with an
     * && operator. Had we reassigned 25 to 'x' we would have run into an error as soon as we wanted to evaluate x && true.
     *
     * Note that Scala is strongly statically typed, so all type information are available at compile-time. This means
     * the Scala compiler can help us much more than a compiler for a dynamically typed language could. In a dynamically
     * typed language, the assignment x = 25 would've been valid, but running the program would have resulted in an
     * exception being thrown.
     *
     * Now you might be wondering why we can write something like
     */
    var y = false
    /*
     * This doesn't mean that 'y' is untyped. However, the Scala compiler is smart enough to figure out on its own that,
     * since 'false' is of type Boolean, the most restrictive type of 'y' is also Boolean. So the compiler computes the
     * type annotation ': Boolean' itself. Ths process is called 'type inference'. However, type inference doesn't work
     * in all cases, which is why sometimes we must explicitly write down type annotations.
     */
    // y = 20 // Even though we didn't specify the type of 'y', the compiler generates the same error as with the reassignment to 'x'

    /*
     * Types are structured hierarchically. This is best presented using an example. I've defined the three types
     * 'Animal', 'Dog', and 'Bird'. The types are designed that the type 'Dog' and the type 'Bird' are both subtypes of
     * 'Animal'. This means that
     * 1. An expression of type 'Animal' can evaluate to any value of type 'Animal' or of a subtype of 'Animal', e.g., 'Dog' or 'Bird'
     * 2. An expression of type 'Bird' cannot evaluate to a value of type 'Animal', because we cannot be sure if it's a 'Bird'
     * 3. Any guarantees given on type 'Animal' must also be given on the types 'Bird' and 'Dog'. In this case, all animals must be able to eat.
     */
    var a: Animal = Dog("Pluto")
    a.eat() // Any animal can eat
    // a.bark() // This won't work, because the type restriction does not give us the guarantee that 'a' can bark, even though we currently know it's a dog
    a = Bird("Tweety") // Naturally, we can also assign a bird
    a.eat() // And the birds can also eat.
    var d = Dog("Goofy")
    d.eat() // Because 'Dog' is a subtype of 'Animal', our dog knows how to eat
    d.bark() // And we also know that our dog can bark
    // d = Bird("Hoothoot") // This certainly won't work, because a bird is not a dog
    a = Dog("Max")
    // d = a // This won't work either, because the type of 'a' is still 'Animal', which isn't guaranteed to be a dog

    Thread.sleep(10) // we're just waiting a short amount of time for the previous statements to finish writing to the console
    /*
     * The topmost type in Scala's type hierarchy is 'Any'. It gives the least guarantees, but every possible value
     * can be assigned to a variable of type 'Any'. However, because 'Any' gives us next to no guarantees it should
     * only rarely be used. Right below 'Any' are 'AnyVal', which is the common supertype to the fundamental numerical
     * values, a supertype of 'Boolean', and some other types. Also right below 'Any' is 'AnyRef' which is the supertype
     * to anything else. Understanding the difference is not important now.
     *
     * There is also a Subtype to all other types called 'Nothing'. An expression of type 'Nothing' cannot take on any
     * value, which really just means evaluating an expression of type 'Nothing' can only lead to exceptional behavior.
     *
     * There's another type at the bottom of the type hierarchy (right above 'Nothing', but restricted to being a subtype
     * of 'AnyRef') called 'Null'. It only accepts the special value 'null', which basically means that no value is
     * present. However, there are much better ways of saying that no value is present, so for now (and whenever possible
     * in the future) avoid using 'null'.
     */
    val n: Null = null
    n.toString // this compiles, but will throw an exception at runtime, because null means no value is present

    /*
     * I strongly recommend reading further material about types as this is such an important concept. However, you
     * may wish to finish lesson 6 first, as we're going to learn about the fundamental types.
     */
  }
}

trait Animal {
  def eat() = println(s"$this is eating")
}

case class Dog(name: String) extends Animal {
  def bark() = println(s"$this is barking")
}

case class Bird(name: String) extends Animal {
  def fly() = println(s"$this is flying")
}
