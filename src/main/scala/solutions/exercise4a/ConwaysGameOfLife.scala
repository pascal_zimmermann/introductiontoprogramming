package solutions.exercise4a

/*
 * In Conway's game of life, we have a two dimensional grid of cells. At each moment in time, each cell is either alive
 * or dead. As time passes, life cells may die because of under-population or over-population, or they may stay alive.
 * At the same time, dead cells may stay dead, or may become live cells.
 * In each iteration, the game follows four rules:
 * - Any live cell with fewer than two live neighbours dies, as if caused by under-population.
 * - Any live cell with two or three live neighbours lives on to the next generation.
 * - Any live cell with more than three live neighbours dies, as if by over-population.
 * - Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
 *
 * See also: https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life
 *
 * In this exercise we will simulate the game of life for three iterations.
 * The cells are represented as an array of arrays with its initial state already provided.
 * We use boolean values to distinguish between live-cells ('true') and dead cells ('false')
 * Instead of Scala's println, you can use the method prettyPrint to display the cells.
 * Live cells are printed as 'X', while dead cells are printed as 'Y'
 * Each cell (y,x) has up to eight neighbours: (y-1,x-1), (y-1, x), (y-1, x+1), (y, x-1), (y, x+1), (y+1, x-1), (y+1, x), (y+1, x+1)
 * Corner-cells have only three neighbours, edge-cells have five neighbours - note that accessing elements out of the bounds of an array throws an exception
 * Remember that setting a value of an array will replace the previous value at this index. Because we need to compute
 * the next iteration based on the current iteration, we must copy the array. Otherwise we would do our computations
 * partly based on the next state.
 *
 * Expected result:
 * Iteration 0:
 * OOXXOO
 * OOXOOO
 * OOOOOO
 * OOXXXO
 * OOOOOO
 *
 * Iteration 1:
 * OOXXOO
 * OOXXOO
 * OOXOOO
 * OOOXOO
 * OOOXOO
 *
 * Iteration 2:
 * OOXXOO
 * OXOOOO
 * OOXOOO
 * OOXXOO
 * OOOOOO
 *
 * Iteration 3:
 * OOXOOO
 * OXOXOO
 * OXXXOO
 * OOXXOO
 * OOOOOO
 */
object ConwaysGameOfLife {
  def main(args: Array[String]) {
    var cells =
      Array(
        Array(false, false, true, true, false, false),
        Array(false, false, true, false, false, false),
        Array(false, false, false, false, false, false),
        Array(false, false, true, true, true, false),
        Array(false, false, false, false, false, false)
      )
    println("Iteration 0:")
    prettyPrint(cells)

    var iteration = 0
    while (iteration < 3) {
      // The result of this iteration must be temporarily stored in a new array, because we must not overwrite the current array
      // Side-note: There are nicer ways to initialize new arrays, but we haven't talked about those so far, hence this initialization to false
      val nextCells =
        Array(
          Array(false, false, false, false, false, false),
          Array(false, false, false, false, false, false),
          Array(false, false, false, false, false, false),
          Array(false, false, false, false, false, false),
          Array(false, false, false, false, false, false)
        )
      // we will iterate over all cells and check how many live-neighbours the cell has
      // Two loops are required, one going from top to bottom, one going from left to right (in the 2d-array)
      var y = 0
      while (y < cells.length) {
        var x = 0
        while (x < cells(y).length) {
          // compute the number of neighbours of this cell (cell(y)(x)), that are alive
          var numberOfLifeNeighbours = 0
          var yn = y - 1
          while (yn <= y + 1) {
            if (yn >= 0 && yn < cells.length) {
              var xn = x - 1
              while (xn <= x + 1) {
                if (xn >= 0 && xn < cells(y).length) {
                  if (xn != x || yn != y) {
                    if (cells(yn)(xn)) {
                      numberOfLifeNeighbours = numberOfLifeNeighbours + 1
                    }
                  }
                }
                xn = xn + 1
              }
            }
            yn = yn + 1
          }
          // In the next iteration, this cell is alive, if either the cell was alive before and two neighbours are alive
          // or if three neighbours are alive, independent of whether this cell was previously alive or dead
          // all other cells are dead
          nextCells(y)(x) = (cells(y)(x) && numberOfLifeNeighbours == 2) || numberOfLifeNeighbours == 3
          x = x + 1
        }
        y = y + 1
      }
      cells = nextCells
      iteration = iteration + 1
      println("Iteration " + iteration + ":")
      prettyPrint(cells)
    }

  }

  /* Prints the cells to the console */
  def prettyPrint(cells: Array[Array[Boolean]]): Unit = {
    def cellToString(cell: Boolean): String = {
      if (cell) "X"
      else "O"
    }
    def lineToString(line: Array[Boolean]): String = {
      line.foldLeft("")((a, b) => a + cellToString(b))
    }
    println(cells.foldLeft("")((a, b) => s"$a${lineToString(b)}\n"))
  }

}
