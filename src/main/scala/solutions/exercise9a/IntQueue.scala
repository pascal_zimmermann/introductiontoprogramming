package solutions.exercise9a

/*
 * Your task is to implement a class IntQueue. Like the class IntStack from lesson 9, IntQueue is a data container for
 * Int values. It also supports the three operations 'isEmpty', 'push', and 'pop' with the same signature as IntStack.
 * The difference however is, that IntStack is a 'first-in-last-out' container, whereas IntQueue is a 'first-in-first-out'
 * container. A real-world analogy would be a queue of people waiting to be served - usually the first person to arrive
 * is the first person to get served.
 *
 * Again we have already prepared a set of tests which are implemented in the main-method. If you run them and nothing is
 * printed to the console, your program is probably working correctly.
 */
object IntQueueTest {
  def main(args: Array[String]): Unit = {
    val queue = new IntQueue
    assertEmpty(queue)
    queue.push(10)
    assertNonEmpty(queue)
    assertEquals(queue.pop(), 10)
    assertEmpty(queue)
    val values = List(1, 4, 2, 5, 1, 5, 6, -1, 23)
    values.foreach(v => queue.push(v))
    values.foreach(v => assertEquals(queue.pop(), v))
    assertEmpty(queue)
  }

  def assertEmpty(queue: IntQueue): Unit = if (!queue.isEmpty) println("Expected queue to be empty, but was not empty")

  def assertNonEmpty(queue: IntQueue): Unit = if (queue.isEmpty) println("Expected queue to be non empty, but was empty")

  def assertEquals(actual: Int, expected: Int): Unit = if (actual != expected) println(s"Expected $expected, but was $actual")
}

class IntQueue {
  var head: Option[IntQueueEntry] = None

  def isEmpty: Boolean = head.isEmpty

  def push(entry: Int): Unit = {
    if (isEmpty) {
      head = Some(new IntQueueEntry(entry, None))
    } else {
      head.get.push(entry)
    }
  }

  def pop(): Int = {
    val result = head.get
    head = result.previous
    result.entry
  }
}

class IntQueueEntry(val entry: Int, var previous: Option[IntQueueEntry]) {
  def push(entry: Int): Unit = {
    if (previous.isEmpty) {
      previous = Some(new IntQueueEntry(entry, None))
    } else {
      previous.get.push(entry)
    }
  }
}
