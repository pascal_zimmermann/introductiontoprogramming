package exercises.exercise6b

/*
 * Given an array of sentences your task is to capitalize each sentence and store it back into the array.
 * A sentence is a sequence of words, separated by a space. They are represented as a String value. A sentence
 * is capitalized if the first character of each word is upper-case.
 *
 * Your code must also be able to handle the empty string, that is, a sentence without words. In this case, the empty
 * string should be returned. If the first character of a word is not [a-z], the same character is supposed to be its
 * upper-case variant.
 * 
 * Expected output:
 * Hello World
 * I Like Strings
 * Water Flows Downwards
 * There's No Problem With Special Characters, Even %-signs Won't Cause My Code To Break.
 *
 * Spaces  Are Also    Fine
 *
 * A sample-solution is provided under solutions.exercise6b
 */
object Capitalize {
  def main(args: Array[String]): Unit = {
    val sentences =
      Array(
        "Hello world",
        "I like strings",
        "water flows downwards",
        "There's no problem with special characters, even %-signs won't cause my code to break.",
        "",
        " spaces  are also    fine"
      )

    /******************
     * Your code here *
     ******************/

    sentences.foreach(println)
  }
}
