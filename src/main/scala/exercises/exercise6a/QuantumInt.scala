package exercises.exercise6a

/*
 * With the rise of quantum computers each bit - called qubit - can now hold
 * not only 2, but 4 states. Since each qubit can hold twice as much information
 * as a bit we can represent much more information using 32 qubits than simply
 * using 32 bits. In fact, with 32 qubits we can store as much information as with
 * 64 bits.
 *
 * In this exercise, a new type QuantumInt has been defined. A QuantumInt is supposed
 * to be a 32-qubit signed integer. Similar to the encoding of the regular old Int,
 * each of the trailing 31 qubits encodes how often we want to add 4^i, where i is
 * 0 for the last qubit, 1 for the last but one qubit, and so on up to 30 for the second
 * qubit. The first qubit can hold the sign and is defined as follows
 * 0 -> 0
 * 1 -> 4611686018427387904
 * 2 -> -4611686018427387904
 * 3 -> -9223372036854775808
 *
 * Have a look at the tests given below if you're not sure about the encoding.
 *
 * Your task is to implement a conversion from QuantumInt to Long. Since 32 qubits hold
 * the same amount of information as 64 bits we can represent exactly the same amount
 * of numbers using Long and using QuantumInt.
 *
 * Hints:
 * - Be aware that Int-values cannot hold enough information, so using Int-values for temporary results may not work
 * - Be aware of overflow - depending on your computation this may cause errors
 *
 * A sample-solution is provided under solutions.exercise6a
 */
object QuantumInt {

  /*
   * The entry point to this exercise will run certain tests to check whether
   * your implementation is correct. You're not supposed to modify code here,
   * but if you wish you may add further calls to assertEquals to add other
   * tests.
   *
   * Run your program to check whether your implementation is correct. If it
   * prints 'Your implementation passed all tests!' you have successfully
   * completed this exercise. Otherwise, the program will output the expected
   * and the actual result, so you can fix the error.
   *
   * Note that commonly you would separate test-code from production code, so
   * you would not write the tests in a main method like we do here. Once we've
   * learned about how to write proper tests we will no longer test our code
   * from within such entry points.
   */
  def main(args: Array[String]): Unit = {
    assertEqual(QuantumInt("00000000000000000000000000000000"), 0L)
    assertEqual(QuantumInt("00000000000000000000000000000001"), 1L)
    assertEqual(QuantumInt("00000000000000000000000000000002"), 2L)
    assertEqual(QuantumInt("00000000000000000000000000000003"), 3L)
    assertEqual(QuantumInt("00000000000000000000000000000010"), 4L)
    assertEqual(QuantumInt("00000000000000000000000000000011"), 5L)
    assertEqual(QuantumInt("01000000000000000000000000000000"), 1152921504606846976L)
    assertEqual(QuantumInt("02000000000000000000000000000000"), 2305843009213693952L)
    assertEqual(QuantumInt("03000000000000000000000000000000"), 3458764513820540928L)
    assertEqual(QuantumInt("30000000000000000000000000000000"), -9223372036854775808L)
    assertEqual(QuantumInt("13333333333333333333333333333333"), 9223372036854775807L)
    assertEqual(QuantumInt("03333333333333333333333333333333"), 4611686018427387903L)
    assertEqual(QuantumInt("10000000000000000000000000000000"), 4611686018427387904L)
    assertEqual(QuantumInt("20000000000000000000000000000000"), -4611686018427387904L)
    assertEqual(QuantumInt("23333333333333333333333333333333"), -1L)
    assertEqual(QuantumInt("23333333333333333333333333333332"), -2L)
    assertEqual(QuantumInt("23333333333333333333333333333331"), -3L)
    assertEqual(QuantumInt("23333333333333333333333333333330"), -4L)
    assertEqual(QuantumInt("23333333333333333333333333333323"), -5L)
    assertEqual(QuantumInt("23333333333333333333333333333312"), -10L)
    assertEqual(QuantumInt("33333333333333333333333333333333"), -4611686018427387905L)
    assertEqual(QuantumInt("01230123012301230123012301230123"), 1953184666628070171L)
    if (success) println("Your implementation passed all tests!")
  }

  def apply(bits: String): QuantumInt = new QuantumInt(bits)

  def assertEqual(qi: QuantumInt, l: Long): Unit = {
    if (qi.toLong != l) {
      success = false
      println(s"The Long alue of the QuantumInt $qi is expected to be $l but it was ${qi.toLong}.")
    }
  }

  var success = true
}

class QuantumInt(qubits: String) {
  /*
   * Convert this QuantumInt to a Long value. The qubits are stored as a String-value which can be accessed as 'qubits'.
   * Any String-operation works, for example you can convert the string to an array of Char using qubits.toCharArray.
   */
  def toLong: Long = {
    var result: Long = 0L
    /******************
     * Your code here *
     ******************/
    result
  }

  override def toString = qubits.foldLeft("")((a, c) => if (a.lastIndexOf(" ") == a.length - 5) a + " " + c else a + c)
}
