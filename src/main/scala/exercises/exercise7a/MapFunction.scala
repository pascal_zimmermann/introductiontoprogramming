package exercises.exercise7a

/*
 * Your task is to define the following four functions:
 * - a function called double that takes an Int and returns the value multiplied by two
 * - a function called abs that takes an Int and returns its absolute value, e.g., abs(4) = 4, abs(-2) = 2, abs(0) = 0
 * - a function called incrementBy that takes an Int and returns a function from Int to Int. The returned function
 *   increments its argument by the value passed to incrementBy, e.g., incrementBy(3)(5) = 8
 * - a higher order function called map that takes a List of Int and a function from Int to Int and returns a List of Int.
 *   map(l, f) is supposed to apply f to every element in l and return a new list containing the results (in the same
 *   order as the input-List), e.g., map(List(2, 4, 6), x => x / 2) = List(1, 2, 3)
 *
 * After you've written your code, uncomment the three lines starting with println to check whether your functions work.
 *
 * A sample-solution is provided under solutions.exercise7a
 */
object MapFunction {
  def main(args: Array[String]): Unit = {

    /******************
     * Your code here *
     ******************/


    /******************
     * Uncomment code *
     ******************/
    // println(map(List(1, 2, 3, 4, 5), incrementBy(3))) // Shall print List(4, 5, 6, 7, 8)
    // println(map(List(1, 2, 3, 4, 5), double)) // Shall print List(2, 4, 6, 8, 10)
    // println(map(List(-1, 2, -5, 2), abs)) // Shall print List(1, 2, 5, 2)
  }
}
