package setup

object Main {
  def main(args: Array[String]) {
    System.out.println(greeting("World"))
  }

  def greeting(target: String): String = "Hello " + target + "!"
}
