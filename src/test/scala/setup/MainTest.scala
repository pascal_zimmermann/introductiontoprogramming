package setup

import org.scalatest.{FlatSpec, ShouldMatchers}

class MainTest extends FlatSpec with ShouldMatchers {
  "Greeting" should "build a greeting message for the target" in {
    Main.greeting("Alice") shouldBe "Hello Alice!"
  }
}
