name := "IntroductionToProgramming"

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  "org.scala-lang" % "scala-reflect" % "2.11.8",
  "org.scala-lang.modules" %% "scala-xml" % "1.0.4",
  "org.scalatest" %% "scalatest" % "2.2.6" % "test"
)

mainClass in (Compile, run) := Some("setup.Main")